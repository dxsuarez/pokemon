//
//  DetailViewController.swift
//  pokemon
//
//  Created by Diego Suárez on 20/6/18.
//  Copyright © 2018 Diego Suárez. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var testLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    var pokemon:Pokemon!
    var image:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = pokemon.name
        weightLabel.text = "\(pokemon.weight)"
        heightLabel.text = "\(pokemon.height)"
        testLabel.text = "Pokemon Detail"
        
        let network = Network()
        network.getImage(pokemon.id) { (image) in
            self.imageView.image = image
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
