import Foundation

struct Pokemon: Decodable {
    var id: Int
    var order: Int
    var name: String
    var weight: Int
    var height: Int
    var sprites: Sprite
}

struct Sprite: Decodable {
    var defaultSprite: String
    
    enum CodingKeys: String, CodingKey {
        case defaultSprite = "front_default"
    }
}
